#pragma once

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <future>
#include <vector>
#include "AudioPluginUtil.h"
#include "SpatializerSubPlugin.h"

#define _USE_MATH_DEFINES
#include <math.h>


// Forward declaration, ignore
class ImageSourceReverb;

/**
* BoxParameters
* Singleton class containing parameters for Image Source Reverb
*/
class BoxParameters
{
private:
	// Default box parameters
	const float defaultBoxPosition[3] = { 0, 0, 0 };
	const float defaultBoxDimensions[3] = { 100, 100, 100 };
	const float defaultBoxReflections[6] = { 1, 1, 1, 1, 1, 1 };
	const float defaultDuration = 0.15;
	const int defaultReflectionOrder = -1; // No limit
public:
	float position[3];
	float dimensions[3];
	float reflections[6];
	float duration;
	int maxreflorder; // -1 == no limit

	// Observer pattern style connection to alert reverbs of changes
	std::vector<ImageSourceReverb*> reverberators;
public:
	BoxParameters();
};

extern "C" __declspec(dllexport)
int ImportBoxParams(float posX, float posY, float posZ,
	float dimX, float dimY, float dimZ,
	float b0, float b1, float b2, float b3, float b4, float b5,
	int delsq, float dur);


class ImageSourceReverb : public SpatializerSubPlugin
{
private:
	struct Channel
	{
		UnityComplexNumber** h;
		UnityComplexNumber** x;
		float* impulse;
		float* s;
	};

	const float sound = 343.0f; // Speed of sound travelling through air at 20C
	
	bool gotImport;

	//Mutex* mutex;
	int numpartitions;
	int fftsize;
	int hopsize;
	int bufferindex;
	int writeoffset;
	int samples;
	UnityComplexNumber* tmpoutput;
	Channel* channels;

	// Previous location matrices to detect movement
	float prvsrcmat[16];
	float prvrcvmat[16];

public:
	BoxParameters * box;

public:
	ImageSourceReverb(UnityAudioEffectState* state, int blocksize, int samplerate);
	~ImageSourceReverb();

	void NotifyChanges();

// SPATIALIZERSUBPLUGIN INTERFACE:
public:
	void UpdateImpulse(UnityAudioEffectState* state);
	void ProcessCallback(UnityAudioEffectState* state, float* inbuffer, float* outbuffer, unsigned int length, int inchannels, int outchannels);

private:
	inline float sinc(float x);
	inline float ToSampleDistance(float meters, int samplerate);
	void GetCoordinates(float* src, float* rcv, UnityAudioEffectState* state);
	void SROOM(float* rcv, float* src, int samplerate);
	void SetupImpulse(UnityAudioEffectState* state, int length);
};