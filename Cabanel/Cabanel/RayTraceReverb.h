#pragma once

#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <windows.h>

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <random>
#include <future>
#include <vector>
#include <exception>
#include <thread>
#include "GeometryPool.h"
#include "AudioPluginUtil.h"
#include "SpatializerSubPlugin.h"
#include "GeometryPlugin.h"

#include "matplotlibcpp.h"

#define _USE_MATH_DEFINES
#include <math.h>

#include "nanort.h"

#include "Misc.h"


// Forward declaration, ignore
class RayTraceReverb;

// Frequency Bands
enum Band { F125, F250, F500, F1K, F2K, F4K, F8K };

/**
* RayTraceSettings
* Singleton class containing settings for Ray Trace Reverb
*/
class RayTraceSettings
{
public:
	float listenerradius;
	int maxreflections;
	int rays;
	float duration; // in seconds

	// Observer pattern style connection to alert reverbs of changes
	std::vector<RayTraceReverb*> reverberators;

	// For debug purposes, used for CabanelRayVisualizer
	bool fetched;
	std::vector<float> calculatedrays;
	std::vector<int> raylengths;
public:
	RayTraceSettings() : listenerradius(1.0), maxreflections(12), rays(10000), duration(1.0) {}

};


class RayTraceReverb : public SpatializerSubPlugin, public GeometryPlugin
{
public:
	struct Channel
	{
		UnityComplexNumber** h;
		UnityComplexNumber** x;
		float* impulse;
		float* s;
	};

	const float sound = 343.0f; // Speed of sound travelling through air at 20C
	
	UnityComplexNumber* tmpoutput;
	Channel channel;

	bool gotImport;

	int numpartitions;
	int fftsize;
	int hopsize;
	int bufferindex;
	int writeoffset;
	int samples;
	int samplerate;

	// DATA LOCK
	std::mutex impulselock;

	// RNG
	std::random_device rd;
	std::mt19937 rndgen;
	std::uniform_real_distribution<> phidis;
	std::uniform_real_distribution<> costhetadis;

	// Previous location matrices to detect movement
	float prvsrcmat[16];
	float prvrcvmat[16];

	bool updateme = false;

public:
	RayTraceSettings * settings;

public:
	RayTraceReverb(UnityAudioEffectState* state, int blocksize);
	~RayTraceReverb();

	void NotifyChanges();

	// SPATIALIZERSUBPLUGIN INTERFACE:
public:
	void UpdateImpulse(UnityAudioEffectState* state);
	void ProcessCallback(UnityAudioEffectState* state, float* inbuffer, float* outbuffer, unsigned int length, int inchannels, int outchannels);

//TODO private
public:
	float AirAbsorbtion(float in, float distance, Band band);
	float SurfaceAbsorption(float in, float coef, float3 inDir, float3 normal);
	inline float ToSampleDistance(float meters, int samplerate);
	inline float3 Reflect(float3 in, float3 surface);
	float3 GetRandomUnitVector();
	bool IntersectsSphere(float3 point, float3 direction, float3 center, float radius);
	void GetCoordinates(UnityAudioEffectState* state, float* src, float* rcv);
	bool DetectMovement(UnityAudioEffectState* state);
	void CalculateImpulse(float3 src, float3 rcv);
	void RayTrace(float3 src, float3 rcv, float* impulse);
	virtual void RayTraceCore(float3 src, float3 rcv, float radius, float* impulse, nanort::BVHAccel<float>& accel, const std::shared_ptr<GeometryPool>& geopool);
};
