#pragma once

#include "RayTraceReverb.h"
#include <set>

class MinTraceReverb : public RayTraceReverb
{
private:
	int raylimit;

	//struct RayVector {
	//	float length;
	//	std::vector<float3> bounces;

	//	void AddBounce(float3 p) {
	//		length += ((p - bounces.back()).length());
	//		bounces.push_back(p);
	//	};
	//};
	struct Impulse
	{
		int sampledistance;
		float energy;

		Impulse(int d, float e) : sampledistance(d), energy(e) {}

		bool operator<(const Impulse& dep) const
		{
			return (sampledistance < dep.sampledistance) || ((sampledistance == dep.sampledistance) && (energy > dep.energy));
		}
	};
public:
	MinTraceReverb(UnityAudioEffectState* state, int blocksize, int _rays)
		: RayTraceReverb(state, blocksize), raylimit(_rays)
	{}
	void RayTraceCore(float3 src, float3 rcv, float* impulse, nanort::BVHAccel<float>& accel, const std::shared_ptr<GeometryPool>& geopool);
};