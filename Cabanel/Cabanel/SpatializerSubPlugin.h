#pragma once

#include "AudioPluginUtil.h"

class SpatializerSubPlugin
{
protected:
	bool active;
public:
	SpatializerSubPlugin() : active(true) {};
	virtual void ToggleActive() 
	{
		active = !active;
	}
	bool IsActive()
	{
		return active;
	}
	void SetActive(bool a)
	{
		active = a;
	}
	virtual void UpdateImpulse(UnityAudioEffectState* state) = 0;
	virtual void ProcessCallback(UnityAudioEffectState* state, float* inbuffer, float* outbuffer, unsigned int length, int inchannels, int outchannels) = 0;
	virtual ~SpatializerSubPlugin() {};
};