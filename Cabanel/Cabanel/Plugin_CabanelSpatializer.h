#pragma once
#include "AudioPluginUtil.h"
#include "GeometryPool.h"
#include "HRTFConvolution.h"
#include "SpatializerSubPlugin.h"
#include "GeometryPlugin.h"
#include "ImageSourceReverb.h"
#include "RayTraceReverb.h"
#include "MinTraceReverb.h"
#include "NullReverb.h"
#include "matplotlibcpp.h"
#include <vector>
#include <map>

#define DLLExport __declspec(dllexport)

namespace Spatializer
{
	enum
	{
		P_AUDIOSRCATTN,
		P_FIXEDVOLUME,
		P_CUSTOMFALLOFF,
		P_FREQ,
		P_MIX,
		P_NUM
	};

	struct EffectData
	{
		float p[P_NUM];
		HRTFConvolution::EffectData hrtfdata;
		std::vector<std::shared_ptr<SpatializerSubPlugin> > subplugins;
		std::map<std::string, std::weak_ptr<SpatializerSubPlugin> > pluginmap;
		std::map<std::string, std::shared_ptr<GeometryPool> > geometrypools;
		float s;
		float c;
		EffectData() {}
	};

	// Access point to state outside of Unity override methods (which provide a pointer)
	// Singleton object
	// Disabled constructor, use State::GetInstance()
	class State
	{
	public:
		UnityAudioEffectState * audioState;
		EffectData* mainData;
		bool started = false;

		static State& GetInstance()
		{
			static State instance;
			return instance;
		}
		State(State const&) = delete;
		void operator=(State const&) = delete;
	private:
		State() {};
	};

	//
	// CABANEL C# INTERFACE QUICK REFERENCE
	// Be sure to read the notes with the function definitions (in the cpp)
	//
	extern "C"
	{
		DLLExport int StartSpatializer();
		DLLExport int StopSpatializer();
		DLLExport int AddImageSourceReverb(const char* pluginid);
		DLLExport int AddRayTraceReverb(const char* pluginid);
		DLLExport int RemovePlugin(const char* pluginid);
		DLLExport int AddGeometryPool(const char* pluginid);
		DLLExport int RemoveGeometryPool(const char* pluginid);
		DLLExport int ImportGeometry(const char* pluginid, float* vertices, int verts, int* triangles, int tris, float coef);
		DLLExport int ClearGeometryPool(const char* pluginid);
		DLLExport int ConnectGeometryPool(const char* pluginid, const char* geometryid);
		DLLExport int UpdateImpulse(const char* pluginid);
		DLLExport int ToggleActive(const char* pluginid);
		DLLExport int SetActive(const char* pluginid, bool active);
		DLLExport int UpdateImageSourceSettings(const char* pluginid,
			float posX, float posY, float posZ,
			float dimX, float dimY, float dimZ,
			float b0, float b1, float b2, float b3, float b4, float b5,
			int delsq, float dur);
		DLLExport int UpdateRayTraceSettings(const char* pluginid, float rad, int maxrefl, int rays, float dur);
		DLLExport int ExportImpulsePlot(const char* pluginid, const char* filename, int xres, int yres);
	}

	void AddSubPlugin(std::string id, const std::shared_ptr<SpatializerSubPlugin>& plugin);
	void RemoveSubPlugin(std::string id);
}