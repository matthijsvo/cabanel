#pragma once
#include<vector>

/**
* GeometryPool
* Singleton class containing arbitrary geometry for Ray Trace Reverb
*/
class GeometryPool
{
public:
	//Tip: use vector.data() to get access to raw pointer
	std::vector<float> vertices;
	std::vector<unsigned int> faces;
	std::vector<float> reflectivity;

	GeometryPool() {}
	~GeometryPool() {}
	void Import(float* vertices, int verts, int* triangles, int tris, float coef);
	void Clear();
	//GeometryPool() {
	//	vertices = {
	//		10, 10, 10,
	//		10, 10, -10,
	//		-10, 10, -10,
	//		-10, 10, 10,
	//		10, -10, 10,
	//		10, -10, -10,
	//		-10, -10, -10,
	//		-10, -10, 10//,
	//		//11, 11, 11,
	//		//11, 11, -11,
	//		//-11, 11, -11,
	//		//-11, 11, 11,
	//		//11, -11, 11,
	//		//11, -11, -11,
	//		//-11, -11, -11,
	//		//-11, -11, 11
	//	};
	//	faces = {
	//		0,4,3,
	//		7,4,3,
	//		5,1,4,
	//		0,1,4,
	//		6,5,2,
	//		1,5,2,
	//		6,7,2,
	//		3,7,2,
	//		6,7,5,
	//		4,7,5,
	//		0,1,3,
	//		2,1,3//,
	//		//0 * 3 + 24,4 * 3 + 24,3 * 3 + 24,
	//		//7 * 3 + 24,4 * 3 + 24,3 * 3 + 24,
	//		//5 * 3 + 24,1 * 3 + 24,4 * 3 + 24,
	//		//0 * 3 + 24,1 * 3 + 24,4 * 3 + 24,
	//		//6 * 3 + 24,5 * 3 + 24,2 * 3 + 24,
	//		//1 * 3 + 24,5 * 3 + 24,2 * 3 + 24,
	//		//6 * 3 + 24,7 * 3 + 24,2 * 3 + 24,
	//		//3 * 3 + 24,7 * 3 + 24,2 * 3 + 24,
	//		//6 * 3 + 24,7 * 3 + 24,5 * 3 + 24,
	//		//4 * 3 + 24,7 * 3 + 24,5 * 3 + 24,
	//		//0 * 3 + 24,1 * 3 + 24,3 * 3 + 24,
	//		//2 * 3 + 24,1 * 3 + 24,3 * 3 + 24
	//	};
	//	reflectivity = {
	//		0.5,
	//		0.5,
	//		0.5,
	//		0.5,
	//		0.5,
	//		0.5,
	//		0.5,
	//		0.5,
	//		0.5,
	//		0.5,
	//		0.5,
	//		0.5
	//	};
	//}
};