// Please note that this will only work on Unity 5.2 or higher.

#include "HRTFConvolution.h"


extern float hrtfSrcData[];
extern float reverbmixbuffer[];

namespace HRTFConvolution
{
	//extra variables for new HRTF
	bool useNewHrtf = false;
	float* hrtfNewData = 0;
	float* hrtfOldData = 0;

	void HRTFData::CircleCoeffs::GetHRTF(UnityComplexNumber* h, float angle)
	{
		// this is under the assumption that azimuth 0 is in hrtftable for every elevation
		float f2 = roundf(angle /(angles[2] - angles[1]));
		int index = (int)f2;

		if (index < 0)
			index = 0;
		else if (index >(numangles - 1))
			index = 0;
		float* hrtf1 = hrtf + HRTFLEN * 4 * index;
		for (int n = 0; n < HRTFLEN * 2; n++)
		{
			h[n].re = hrtf1[0];
			h[n].im = hrtf1[1];
			hrtf1 += 2;
		}
	}

	HRTFData::HRTFData()
    {
        float* p = useNewHrtf ? hrtfNewData : hrtfSrcData;
		d_angle = (float)(*p++);
		n_el = (int)(*p++);
        for (int c = 0; c < 2; c++)
        {
            for (int e = 0; e < n_el; e++)
            {
                CircleCoeffs& coeffs = hrtfChannel[c][e];
                coeffs.numangles = (int)(*p++);
                coeffs.angles = p;
                p += coeffs.numangles;
                coeffs.hrtf = new float[coeffs.numangles * HRTFLEN * 4];
                float* dst = coeffs.hrtf;
                UnityComplexNumber h[HRTFLEN * 2];
                for (int a = 0; a < coeffs.numangles; a++)
                {
                    memset(h, 0, sizeof(h));
                    for (int n = 0; n < HRTFLEN; n++)
                        h[n + HRTFLEN].re = p[n];
                    p += HRTFLEN;
                    FFT::Forward(h, HRTFLEN * 2, false);
                    for (int n = 0; n < HRTFLEN * 2; n++)
                    {
                        *dst++ = h[n].re;
                        *dst++ = h[n].im;
                    }
                }
            }
        }
    }

	HRTFData sharedDataNew;

	//callback for Unity to update the HRTF
	extern "C" UNITY_AUDIODSP_EXPORT_API void Spatializer_UpdateHrtf(float* hrtfData, int size)
	{
		//hrtf array size can differ, create new array with new size and copy data into it
		float* newArr = new float[size];
		memcpy(newArr, hrtfData, size * sizeof(float));
		
		//delete the old array and point to new, we keep hrtfOldData alive as a buffer
		delete[] hrtfOldData;
		hrtfOldData = hrtfNewData;
		hrtfNewData = newArr;

		//flag indicating we are using new HRTF
		useNewHrtf = true;

		//rebuild HRTFData using new HRTF
		sharedDataNew = HRTFConvolution::HRTFData();
	}

	//callback for Unity to update the HRTF
	extern "C" UNITY_AUDIODSP_EXPORT_API void Spatializer_DefaultHrtf()
	{
		useNewHrtf = false;
	}

    void Setup(UnityAudioEffectState* state, EffectData* effectdata)
    {
        effectdata = new EffectData;
        //memset(effectdata, 0, sizeof(EffectData));   
    }

	void GetHRTF(int channel, UnityComplexNumber* h, float azimuth, float elevation)
	{
		HRTFConvolution::HRTFData sharedDataTmp = (useNewHrtf ? sharedDataNew : sharedData);
		float e = max(elevation*1.0f + 90.0, 0);
		float f = roundf(e / sharedData.d_angle);
		int index1 = (int)f;
		if (index1 < 0)
			index1 = 0;		
		else if (index1 >(sharedData.n_el - 1))
			index1 = sharedData.n_el - 1;

		sharedDataTmp.hrtfChannel[channel][index1].GetHRTF(h, azimuth);
    }

    void ProcessCallback(UnityAudioEffectState* state, EffectData* data, float* inbuffer, float* outbuffer, unsigned int length, int inchannels, int outchannels)
    {
        // Check that I/O formats are right and that the host API supports this feature
        if (inchannels != 2 || outchannels != 2 ||
            state->spatializerdata == NULL)
        {
            memcpy(outbuffer, inbuffer, length * outchannels * sizeof(float));
            return;
        }

        static const float kRad2Deg = 180.0f / kPI;

        float* m = state->spatializerdata->listenermatrix;
        float* s = state->spatializerdata->sourcematrix;

        // Currently we ignore source orientation and only use the position
        float px = s[12];
        float py = s[13];
        float pz = s[14];

        float dir_x = m[0] * px + m[4] * py + m[8] * pz + m[12];
        float dir_y = m[1] * px + m[5] * py + m[9] * pz + m[13];
        float dir_z = m[2] * px + m[6] * py + m[10] * pz + m[14];

        float azimuth = (fabsf(dir_z) < 0.001f) ? 0.0f : atan2f(dir_x, dir_z);
        if (azimuth < 0.0f)
            azimuth += 2.0f * kPI;
        azimuth = FastClip(azimuth * kRad2Deg, 0.0f, 360.0f);

        float elevation = atan2f(dir_y, sqrtf(dir_x * dir_x + dir_z * dir_z) + 0.001f) * kRad2Deg;
        float spatialblend = state->spatializerdata->spatialblend;
        float reverbmix = state->spatializerdata->reverbzonemix;

        GetHRTF(0, data->ch[0].h, azimuth, elevation);
        GetHRTF(1, data->ch[1].h, azimuth, elevation);

        // From the FMOD documentation:
        //   A spread angle of 0 makes the stereo sound mono at the point of the 3D emitter.
        //   A spread angle of 90 makes the left part of the stereo sound place itself at 45 degrees to the left and the right part 45 degrees to the right.
        //   A spread angle of 180 makes the left part of the stero sound place itself at 90 degrees to the left and the right part 90 degrees to the right.
        //   A spread angle of 360 makes the stereo sound mono at the opposite speaker location to where the 3D emitter should be located (by moving the left part 180 degrees left and the right part 180 degrees right). So in this case, behind you when the sound should be in front of you!
        // Note that FMOD performs the spreading and panning in one go. We can't do this here due to the way that impulse-based spatialization works, so we perform the spread calculations on the left/right source signals before they enter the convolution processing.
        // That way we can still use it to control how the source signal downmixing takes place.
        float spread = cosf(state->spatializerdata->spread * kPI / 360.0f);
        float spreadmatrix[2] = { 2.0f - spread, spread };

        float* reverb = reverbmixbuffer;
        for (int sampleOffset = 0; sampleOffset < length; sampleOffset += HRTFLEN)
        {
            for (int c = 0; c < 2; c++)
            {
                // stereopan is in the [-1; 1] range, this acts the way fmod does it for stereo
                float stereopan = 1.0f - ((c == 0) ? FastMax(0.0f, state->spatializerdata->stereopan) : FastMax(0.0f, -state->spatializerdata->stereopan));

                InstanceChannel& ch = data->ch[c];

                for (int n = 0; n < HRTFLEN; n++)
                {
                    float left  = inbuffer[n * 2];
                    float right = inbuffer[n * 2 + 1];
                    ch.buffer[n] = ch.buffer[n + HRTFLEN];
                    ch.buffer[n + HRTFLEN] = left * spreadmatrix[c] + right * spreadmatrix[1 - c];
                }

                for (int n = 0; n < HRTFLEN * 2; n++)
                {
                    ch.x[n].re = ch.buffer[n];
                    ch.x[n].im = 0.0f;
                }

                FFT::Forward(ch.x, HRTFLEN * 2, false);

                for (int n = 0; n < HRTFLEN * 2; n++)
                    UnityComplexNumber::Mul<float, float, float>(ch.x[n], ch.h[n], ch.y[n]);

                FFT::Backward(ch.y, HRTFLEN * 2, false);

                for (int n = 0; n < HRTFLEN; n++)
                {
                    float s = inbuffer[n * 2 + c] * stereopan;
                    float y = s + (ch.y[n].re * GAINCORRECTION - s) * spatialblend;
                    outbuffer[n * 2 + c] = y;
                    reverb[n * 2 + c] += y * reverbmix;
                }
            }

            inbuffer += HRTFLEN * 2;
            outbuffer += HRTFLEN * 2;
            reverb += HRTFLEN * 2;
        }
    }
}
