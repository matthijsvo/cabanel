#include "GeometryPool.h"

void GeometryPool::Import(float* vertarr, int verts, int* triangles, int tris, float coef)
{
	int oldSize = vertices.size();

	for (int i = 0; i < verts; ++i)
	{
		vertices.push_back(vertarr[i]);
	}

	// Append all indices with offset (to compensate for old vertices already in vector)
	for (int i = 0; i < tris; ++i)
	{
		faces.push_back(oldSize + triangles[i]);
	}

	for (int i = 0; i < tris / 3; ++i)
	{
		reflectivity.push_back(coef);
	}
}

void GeometryPool::Clear()
{
	vertices.clear();
	faces.clear();
	reflectivity.clear();
}