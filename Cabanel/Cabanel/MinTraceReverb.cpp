#include "MinTraceReverb.h"

void MinTraceReverb::RayTraceCore(float3 src, float3 rcv, float* impulse, nanort::BVHAccel<float>& accel, const std::shared_ptr<GeometryPool>& geopool)
{
	const float tFar = 1.0e+30f;
	nanort::BVHTraceOptions trace_options;
	//float initialenergy = pow(10, 0.1 * 50/*dB*/) / settings->rays;
	float initialenergy = 0.1f;

	std::set<Impulse> bestImpulses;

	for (int i = 0; i < settings->rays; ++i)
	{
		// Start at source in a random diretion
		float3 pos(src);
		float3 dir = GetRandomUnitVector();
		float energy = initialenergy;
		float totaldist = 0;
		bool received = false;
		std::vector<float> bounces;

		for (int j = 0; j < settings->maxreflections; ++j)
		{
			// If energy too low, quit
			if (energy < initialenergy / 1000000) { // decay of 60dB
				break;
			}

			nanort::Ray<float> ray;
			ray.min_t = 0.00001f;
			ray.max_t = tFar;
			pos.copyTo(ray.org);
			dir.copyTo(ray.dir);

			nanort::TriangleIntersector<> triangle_intersector(geopool->vertices.data(), geopool->faces.data(), sizeof(float) * 3);
			nanort::TriangleIntersection<> isect;

			if (!accel.Traverse(ray, triangle_intersector, &isect, trace_options))
			{
				// Most likely an aliasing/corner issue, or geometry is not closed
				// Better start a new ray
				break;
			}

			// Check if ray went through listener
			if (IntersectsSphere(pos, dir, rcv, settings->listenerradius))
			{
				float rcvdist = abs((rcv - pos).length() - settings->listenerradius);
				float hitdist = isect.t;
				if (rcvdist <= hitdist)
				{
					// RAY HIT LISTENER

					totaldist += rcvdist;
					energy = AirAbsorbtion(energy, rcvdist, F1K);

					int samplepos = (int) ToSampleDistance(totaldist, samplerate);
					if (samplepos >= samples)
					{
						// Doesn't fit in samples anymore unfortunately
						//throw std::exception("Coranon Silaria, Oozu Mahoke!");
						break;
					}

					// Keep only the closest impulses
					if (bestImpulses.size() <= raylimit)
					{
						bestImpulses.insert(Impulse(samplepos, energy));
					}
					else
					{
						auto biggest = std::prev(bestImpulses.end());
						if (biggest->sampledistance > samplepos)
						{
							bestImpulses.erase(biggest);
							bestImpulses.insert(Impulse(samplepos, energy));
						}
					}
					break;
				}
			}

			float dist = isect.t;
			totaldist += dist;

			if (ToSampleDistance(totaldist, samplerate) >= samples) {
				// Even if ray reaches receiver afterwards, it won't fit in the impulse response anyway
				break;
			}

			float3 hitPos = pos + dir * dist;

			// Keep hit position stored in bounces
			bounces.push_back(hitPos.x);
			bounces.push_back(hitPos.y);
			bounces.push_back(hitPos.z);

			// Calculate surface normal of hit triangle
			float3 norm(0, 0, 0);
			unsigned int primId = isect.prim_id;

			GeometryPool* gp = geopool.get();

			unsigned int i0 = gp->faces[3 * primId + 0];
			unsigned int i1 = gp->faces[3 * primId + 1];
			unsigned int i2 = gp->faces[3 * primId + 2];

			float3 p1(&gp->vertices[i0 * 3]);
			float3 p2(&gp->vertices[i1 * 3]);
			float3 p3(&gp->vertices[i2 * 3]);
			float3 v = p2 - p1;
			float3 w = p3 - p1;

			norm.x = v.y * w.z - v.z * w.y;
			norm.y = v.z * w.x - v.x * w.z;
			norm.z = v.x * w.y - v.y * w.x;
			norm.normalize();

			// If surface normal faces wrong direction (angle between normal and direction <= 90 degrees), flip it
			if (vdot(norm, dir) > 0)
			{
				norm *= -1;
			}

			// Calculate energy loss
			energy = AirAbsorbtion(energy, dist, F1K);
			energy = SurfaceAbsorption(energy, geopool->reflectivity[primId], dir, norm);

			// Get reflected direction
			float3 reflDir = Reflect(dir, norm);

			// Update position, direction for next ray segment
			pos = hitPos;
			dir = reflDir;
		}
	}

	for (auto it = bestImpulses.begin(); it != bestImpulses.end(); ++it)
	{
		impulse[it->sampledistance] = it->energy;
	}
}