#pragma once
#include "Misc.h"

float3::float3() 
{}

float3::float3(float xx, float yy, float zz) 
{
	x = xx;
	y = yy;
	z = zz;
}

float3::float3(const float *p) 
{
	x = p[0];
	y = p[1];
	z = p[2];
}

float3 float3::operator-(const float3 &f2) const 
{
	return float3(x - f2.x, y - f2.y, z - f2.z);
}

float3 float3::operator-() const 
{ 
	return float3(-x, -y, -z); 
}

float3 float3::operator*(float f) const
{
	return float3(x * f, y * f, z * f);
}

float3 float3::operator*(const float3 &f2) const 
{
	return float3(x * f2.x, y * f2.y, z * f2.z);
}

float3 float3::operator+(const float3 &f2) const 
{
	return float3(x + f2.x, y + f2.y, z + f2.z);
}

float3& float3::operator+=(const float3 &f2)
{
	x += f2.x;
	y += f2.y;
	z += f2.z;
	return (*this);
}

float3& float3::operator*=(const float3 &f2) 
{
	x *= f2.x;
	y *= f2.y;
	z *= f2.z;
	return (*this);
}

float3& float3::operator*=(const float &f2) 
{
	x *= f2;
	y *= f2;
	z *= f2;
	return (*this);
}

float3 float3::operator/(const float3 &f2) const 
{
	return float3(x / f2.x, y / f2.y, z / f2.z);
}

float3 float3::operator/(const float &f2) const 
{
	return float3(x / f2, y / f2, z / f2);
}

float float3::operator[](int i) const 
{
	return (&x)[i]; 
}

float& float3::operator[](int i) 
{
	return (&x)[i];
}

float3 float3::neg()
{ 
	return float3(-x, -y, -z); 
}

float float3::length() 
{ 
	return sqrtf(x * x + y * y + z * z); 
}

void float3::copyFrom(const float* vec)
{
	x = vec[0];
	y = vec[1];
	z = vec[2];
}

void float3::copyTo(float* vec) 
{
	vec[0] = x;
	vec[1] = y;
	vec[2] = z;
}


void float3::normalize() 
{
	float len = length();
	if (fabs(len) > 1.0e-6)
	{
		float inv_len = 1.0 / len;
		x *= inv_len;
		y *= inv_len;
		z *= inv_len;
	}
}

std::ostream& operator<<(std::ostream& os, const float3& obj) {
	return os << "(" << obj.x << ", " << obj.y << ", " << obj.z << ")";
}
