#pragma once

#include "AudioPluginUtil.h"


namespace HRTFConvolution
{
	const int HRTFLEN = 512;

	const float GAINCORRECTION = 2.0f;
	const float DANGLE = 10.0;

	class HRTFData
	{
		struct CircleCoeffs
		{
			int numangles;
			float* hrtf;
			float* angles;

			void GetHRTF(UnityComplexNumber* h, float angle);
		};

	public:
		CircleCoeffs hrtfChannel[2][62];
		int n_el;
		float d_angle;

	public:
		HRTFData();
	};

	static HRTFData sharedData;

	struct InstanceChannel
	{
		UnityComplexNumber h[HRTFLEN * 2];
		UnityComplexNumber x[HRTFLEN * 2];
		UnityComplexNumber y[HRTFLEN * 2];
		float buffer[HRTFLEN * 2];
	};

	struct EffectData
	{
		//float p[P_NUM];
		InstanceChannel ch[2];
	};

	//callback for Unity to update the HRTF
	extern "C" UNITY_AUDIODSP_EXPORT_API void Spatializer_UpdateHrtf(float* hrtfData, int size);

	//callback for Unity to update the HRTF
	extern "C" UNITY_AUDIODSP_EXPORT_API void Spatializer_DefaultHrtf();

	void Setup(UnityAudioEffectState* state, EffectData* effectdata);
	
	void GetHRTF(int channel, UnityComplexNumber* h, float azimuth, float elevation);
	
	void ProcessCallback(UnityAudioEffectState* state, EffectData* data, float* inbuffer, float* outbuffer, unsigned int length, int inchannels, int outchannels);
}