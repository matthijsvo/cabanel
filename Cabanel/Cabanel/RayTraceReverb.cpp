#include "RayTraceReverb.h"

RayTraceReverb::RayTraceReverb(UnityAudioEffectState* state, int blocksize)
{
	bufferindex = 0;
	writeoffset = 0;
	hopsize = blocksize;
	fftsize = blocksize * 2;
	tmpoutput = new UnityComplexNumber[fftsize];
	channel = Channel();
	samplerate = state->samplerate;
	settings = new RayTraceSettings();

	// RNG
	rndgen = std::mt19937(rd());
	phidis = std::uniform_real_distribution<>(0.0, 2.0 * M_PI);
	costhetadis = std::uniform_real_distribution<>(-1.0, 1.0);

	// Calculate amount of partitions and samples
	numpartitions = 0;
	while (numpartitions * hopsize < settings->duration * samplerate)
		numpartitions++;
	samples = numpartitions * hopsize;

	// Prepare channel
	Channel& c = channel;

	// New impulse array, initialized as zeroes
	c.impulse = new float[samples];
	memset(c.impulse, 0, sizeof(float) * samples);
		
	// Set first to 1 by default (no RIR effect)
	// This is supposed to be updated later when we have access to geometry and can ray trace
	c.impulse[0] = 1;

	// Prepare for FFT later
	c.s = new float[fftsize];
	memset(c.s, 0, sizeof(float) * fftsize);

	//c.h = new UnityComplexNumber*[numpartitions];
	//c.x = new UnityComplexNumber*[numpartitions];
	//float* s = c.impulse;
	//for (int k = 0; k < numpartitions; k++)
	//{
	//	c.h[k] = new UnityComplexNumber[fftsize];
	//	c.x[k] = new UnityComplexNumber[fftsize];
	//	memset(c.x[k], 0, sizeof(UnityComplexNumber) * fftsize);
	//	memset(c.h[k], 0, sizeof(UnityComplexNumber) * fftsize);
	//	for (int n = 0; n < hopsize; n++)
	//		c.h[k][n].re = *s++;
	//	FFT::Forward(c.h[k], fftsize, false);
	//}
}

RayTraceReverb::~RayTraceReverb() 
{
	delete tmpoutput;
	delete settings;
}

void RayTraceReverb::NotifyChanges()
{
	gotImport = true;
}

float RayTraceReverb::AirAbsorbtion(float in, float distance, Band band)
{
	float coef;
	switch (band) {
	case F125:
		coef = 0.0001f;
		break;
	case F250:
		coef = 0.0003f;
		break;
	case F500:
		coef = 0.0006f;
		break;
	case F1K:
		coef = 0.0011f;
		break;
	case F2K:
		coef = 0.0017f;
		break;
	case F4K:
		coef = 0.0035f;
		break;
	case F8K:
		coef = 0.0106f;
		break;
	}
	return in * exp(-coef * distance);
}

float RayTraceReverb::SurfaceAbsorption(float in, float coef, float3 inDir, float3 normal)
{
	return in * coef;
}

/**
* ToSampleDistance
* Converts distances in meters to distances in sample lengths
*/
inline float RayTraceReverb::ToSampleDistance(float meters, int samplerate)
{
	return meters / (sound / (float)samplerate);
}

/**
* Reflect
* Takes incident and surface normal vectors
* Returns reflected direction according to Snell's Law (in vector form)
* https://en.wikipedia.org/wiki/Specular_reflection#Vector_formulation
*/
inline float3 RayTraceReverb::Reflect(float3 in, float3 surface)
{
	return in - 2 * vdot(in, surface) * surface;
}

/**
* GetRandomUnitVector
* Generates randomized unit vector
* Used to send rays in random directions from sound sources
*/
float3 RayTraceReverb::GetRandomUnitVector()
{
	float phi = phidis(rndgen);
	float theta = acos(costhetadis(rndgen));
	return float3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
}

/**
* IntersectsSphere
* Checks whether a line (defined by a point and a direction vector) will intersect
* a sphere (defined by a center point and a radius)
* Used to check whether a ray intersects with the listener
*
* Adapted from 'Real Time Collision Detection', Christer Ericson, 2004
* Section 5.3.2 'Intersecting Ray or Segment Against Sphere'
*/
bool RayTraceReverb::IntersectsSphere(float3 point, float3 direction, float3 center, float radius)
{
	float3 m = point - center;
	float c = vdot(m, m) - radius * radius;

	// If there is definitely at least one real root, there must be an intersection
	if (c <= 0.0f) return true;

	float b = vdot(m, direction);

	// Early exit if ray origin outside sphere and ray pointing away from sphere
	if (b > 0.0f) return false;

	float disc = b * b - c;

	// A negative discriminant corresponds to ray missing sphere
	if (disc < 0.0f) return false;

	// Now ray must hit sphere
	return true;
}

/**
* GetCoordinates
* Calculates relative coordinates of audio source and audio listener/receiver within the shoebox
* and inserts them in respectively given 'src' and 'rcv' arrays. These are expected to be 3 floats long.
*/
void RayTraceReverb::GetCoordinates(UnityAudioEffectState* state, float* src, float* rcv)
{
	float* srcM = state->spatializerdata->sourcematrix;
	float* rcvM = state->spatializerdata->listenermatrix;
	src[0] = srcM[12];
	src[1] = srcM[13];
	src[2] = srcM[14];
	rcv[0] = -(rcvM[0] * rcvM[12] + rcvM[1] * rcvM[13] + rcvM[2] * rcvM[14]);
	rcv[1] = -(rcvM[4] * rcvM[12] + rcvM[5] * rcvM[13] + rcvM[6] * rcvM[14]);
	rcv[2] = -(rcvM[8] * rcvM[12] + rcvM[9] * rcvM[13] + rcvM[10] * rcvM[14]);
}

/**
* DetectMovement
* Checks if position matrices in the current state are identical to the previous ones
*/
bool RayTraceReverb::DetectMovement(UnityAudioEffectState* state)
{
	for (int i = 0; i < 16; ++i) {
		if (prvsrcmat[i] != state->spatializerdata->sourcematrix[i] || prvrcvmat[i] != state->spatializerdata->listenermatrix[i]) {
			return true;
		}
	}
	return false;
}

/**
* CalculateImpulse
* Recalculate RIR
* Be sure to use this method to recalculate the RIR, not RayTrace separately!
*/
void RayTraceReverb::CalculateImpulse(float3 src, float3 rcv)
{
	// Prepare channels
	Channel& c = channel;

	// New impulse array, initialized as zeroes
	c.impulse = new float[samples];
	memset(c.impulse, 0, sizeof(float) * samples);

	// Prepare for FFT later
	c.s = new float[fftsize];
	memset(c.s, 0, sizeof(float) * fftsize);

	// Use temporary working array, so channels don't need to be mutex locked during calculation
	float* workarray = new float[samples];
	memset(workarray, 0, sizeof(float) * samples);

	RayTrace(src, rcv, workarray);

	// Copy impulse response back to proper channels
	impulselock.lock();
	memcpy(channel.impulse, workarray, samples);
	
	delete workarray;

	// Partition the impulse response in preparation of convolution
	c.h = new UnityComplexNumber*[numpartitions];
	c.x = new UnityComplexNumber*[numpartitions];
	float* s = c.impulse;
	for (int k = 0; k < numpartitions; k++)
	{
		c.h[k] = new UnityComplexNumber[fftsize];
		c.x[k] = new UnityComplexNumber[fftsize];
		memset(c.x[k], 0, sizeof(UnityComplexNumber) * fftsize);
		memset(c.h[k], 0, sizeof(UnityComplexNumber) * fftsize);
		for (int n = 0; n < hopsize; n++)
			c.h[k][n].re = *s++;
		FFT::Forward(c.h[k], fftsize, false);
	}
	impulselock.unlock();
}

/**
* RayTrace
* Uses Ray/Path Tracing method to calculate Room Impulse Response, places impulse response in array 'impulse'
* IMPORTANT: 'impulse' needs to be properly ALLOCATED and have the right SIZE (see member 'samples')!
*/
void RayTraceReverb::RayTrace(float3 src, float3 rcv, float* impulse) 
{
	if (std::shared_ptr<GeometryPool> geopool = geometry.lock())
	{
		// If no geometry received yet, do nothing
		if (geopool->vertices.empty())
		{
			impulse[0] = 1;
			return;
		}

		// NanoRT setup
		bool ret = false;
		nanort::BVHBuildOptions<float> build_options; // Use default option
		nanort::TriangleMesh<float> triangle_mesh(geopool->vertices.data(), geopool->faces.data(), sizeof(float) * 3);
		nanort::TriangleSAHPred<float> triangle_pred(geopool->vertices.data(), geopool->faces.data(), sizeof(float) * 3);
		nanort::BVHAccel<float> accel;
		ret = accel.Build(geopool->faces.size() / 3, triangle_mesh, triangle_pred, build_options);
		if (ret != true)
			throw std::runtime_error("NanoRT: Failed to build BVH!");
		nanort::BVHBuildStatistics stats = accel.GetStatistics();

		printf("  BVH statistics:\n");
		printf("    # of leaf   nodes: %d\n", stats.num_leaf_nodes);
		printf("    # of branch nodes: %d\n", stats.num_branch_nodes);
		printf("  Max tree depth     : %d\n", stats.max_tree_depth);
		float bmin[3], bmax[3];
		accel.BoundingBox(bmin, bmax);
		printf("  Bmin               : %f, %f, %f\n", bmin[0], bmin[1], bmin[2]);
		printf("  Bmax               : %f, %f, %f\n", bmax[0], bmax[1], bmax[2]);

		// Spherical receiver size formula derived from Xiangyang et al., 2003
		float radius = settings->listenerradius;
		if (settings->listenerradius == -1)
		{
			float boxvolume = (bmax[0] - bmin[0]) * (bmax[1] - bmin[1]) * (bmax[2] - bmin[2]);
			float rcvvolume = log10(boxvolume) * (rcv - src).length() * sqrt(4.0 / (float)settings->rays);
			radius = cbrt(rcvvolume / ((4 / 3) * M_PI));
		}

		// Split up for easier custom functionality in derived classes
		RayTraceCore(src, rcv, radius, impulse, accel, geopool);

		// Export RIR to log for debugging purposes
		//std::ofstream logfile("cabanel_rt_rir.log");
		//if (logfile.is_open())
		//{
		//	for (int i = 0; i < samples; ++i) {
		//		logfile << i << " " << impulse[i] << "\n";
		//	}
		//}

		//std::vector<float> plotvec(impulse, impulse + 1000);
		//matplotlibcpp::figure_size(1200, 780);
		//matplotlibcpp::plot(plotvec);
		//matplotlibcpp::save("./impulse.png");
	}
	else
	{
		throw std::runtime_error("Tracer connected to invalid/nonexistent geometry pool!");
	}
}


/** RayTraceCore
 * Internals of ray tracing. Made a separate function to override in child classes with custom functionality.
 */
void RayTraceReverb::RayTraceCore(float3 src, float3 rcv, float radius, float* impulse, nanort::BVHAccel<float>& accel, const std::shared_ptr<GeometryPool>& geopool)
{
	const float tFar = 1.0e+30f;
	nanort::BVHTraceOptions trace_options;
	//float initialenergy = pow(10, 0.1 * 50/*dB*/) / settings->rays;
	float initialenergy = 1.0;

	// Reset bounce points from previous ray tracing
	settings->fetched = false;
	settings->calculatedrays.clear();
	settings->raylengths.clear();

	std::ofstream logfile("cabanel_tracelog.log"); // DEBUG
	logfile << "TOTAL RAYS: " << settings->rays << " | TOTAL BOUNCES " << settings->maxreflections << " | SOURCE " << src << " | LISTENER " << rcv << " | RADIUS " << radius << "\n";
	for (int i = 0; i < settings->rays; ++i)
	{
		//logfile << "come on now\n";
		// Start at source in a random diretion
		float3 pos(src);
		float3 dir = GetRandomUnitVector();
		float energy = initialenergy;
		float totaldist = 0;
		bool received = false;
		std::vector<float> bounces;

		for (int j = 0; j < settings->maxreflections; ++j)
		{
			// If energy too low, quit
			if (energy < initialenergy / 1000000) { // decay of 60dB
				//logfile << "RAY " << i << " DEPLETED after " << j << " BOUNCES\n";
				break;
			}
			//logfile << "RAY " << i << ": " << pos << " BOUNCES\n";
			nanort::Ray<float> ray;
			ray.min_t = 0.00001f;
			ray.max_t = tFar;
			pos.copyTo(ray.org);
			dir.copyTo(ray.dir);

			nanort::TriangleIntersector<> triangle_intersector(geopool->vertices.data(), geopool->faces.data(), sizeof(float) * 3);
			nanort::TriangleIntersection<> isect;

			if (!accel.Traverse(ray, triangle_intersector, &isect, trace_options))
			{
				// Most likely an aliasing/corner issue, or geometry is not closed
				// Better start a new ray
				//logfile << "RAY " << i << " ENTERED THE RATCHET REALM after " << j << " BOUNCES\n";
				break;
			}

			// Check if ray went through listener
			if (IntersectsSphere(pos, dir, rcv, radius))
			{
				float rcvdist = abs((rcv - pos).length() - radius);
				float hitdist = isect.t;
				if (rcvdist <= hitdist)
				{
					// RAY HIT LISTENER

					// Register bounce positions
					settings->raylengths.push_back(j + 1);
					settings->calculatedrays.insert(std::end(settings->calculatedrays), std::begin(bounces), std::end(bounces));
					settings->calculatedrays.push_back(rcv.x);
					settings->calculatedrays.push_back(rcv.y);
					settings->calculatedrays.push_back(rcv.z);

					totaldist += rcvdist;
					energy = AirAbsorbtion(energy, rcvdist, F1K);

					int samplepos = ToSampleDistance(totaldist, samplerate);
					if (samplepos >= samples)
					{
						// Doesn't fit in samples anymore unfortunately
						//throw std::exception("Coranon Silaria, Oozu Mahoke!");
						//logfile << "RAY " << i << " LANDED TOO LATE after " << j << " BOUNCES\n";
						break;
					}
					//logfile << "RAY " << i << " HIT after " << j << " BOUNCES with " << energy << " ENERGY\n";
					impulse[samplepos] = energy;
					break;
				}
			}

			float dist = isect.t;
			totaldist += dist;

			if (ToSampleDistance(totaldist, samplerate) >= samples) {
				// Even if ray reaches receiver afterwards, it won't fit in the impulse response anyway
				break;
			}

			float3 hitPos = pos + dir * dist;

			// Keep hit position stored in bounces
			bounces.push_back(hitPos.x);
			bounces.push_back(hitPos.y);
			bounces.push_back(hitPos.z);

			// Calculate surface normal of hit triangle
			float3 norm(0, 0, 0);
			unsigned int primId = isect.prim_id;

			GeometryPool* gp = geopool.get();

			unsigned int i0 = gp->faces[3 * primId + 0];
			unsigned int i1 = gp->faces[3 * primId + 1];
			unsigned int i2 = gp->faces[3 * primId + 2];

			float3 p1(&gp->vertices[i0 * 3]);
			float3 p2(&gp->vertices[i1 * 3]);
			float3 p3(&gp->vertices[i2 * 3]);
			float3 v = p2 - p1;
			float3 w = p3 - p1;

			norm.x = v.y * w.z - v.z * w.y;
			norm.y = v.z * w.x - v.x * w.z;
			norm.z = v.x * w.y - v.y * w.x;
			norm.normalize();

			// If surface normal faces wrong direction (angle between normal and direction <= 90 degrees), flip it
			if (vdot(norm, dir) > 0)
			{
				norm *= -1;
			}

			// Calculate energy loss
			energy = AirAbsorbtion(energy, dist, F1K);
			energy = SurfaceAbsorption(energy, 0.5/*geometry->reflectivity[primId]*/, dir, norm);

			// Get reflected direction
			float3 reflDir = Reflect(dir, norm);

			// Update position, direction for next ray segment
			pos = hitPos;
			dir = reflDir;
		}
		//logfile << "RAY " << i << " DIED\n";
	}
	logfile.close(); // DEBUG
}


void RayTraceReverb::UpdateImpulse(UnityAudioEffectState* state)
{
	float src[3];
	float rcv[3];
	GetCoordinates(state, src, rcv);
	std::thread t1(&RayTraceReverb::CalculateImpulse, this, src, rcv);
	t1.detach();
	//updateme = true;
}


void RayTraceReverb::ProcessCallback(UnityAudioEffectState* state, float* inbuffer, float* outbuffer, unsigned int length, int inchannels, int outchannels)
{
	const float wet = 30.0f * 0.01f;
	const float gain = powf(10.0f, 0.05f);

	if (gotImport) 
	{
		// Recalculate samples, partitions
		numpartitions = 0;
		while (numpartitions * hopsize < settings->duration * state->samplerate)
			numpartitions++;
		samples = numpartitions * hopsize;

		float rcv[3];
		float src[3];
		GetCoordinates(state, src, rcv);
		CalculateImpulse(src, rcv);
		std::copy(state->spatializerdata->sourcematrix, state->spatializerdata->sourcematrix + 16,
			prvsrcmat);
		std::copy(state->spatializerdata->listenermatrix, state->spatializerdata->listenermatrix + 16,
			prvrcvmat);
		gotImport = false;
	}
	//else if(DetectMovement(state))
	//{
	//	float rcv[3];
	//	float src[3];
	//	GetCoordinates(state, src, rcv);
	//	CalculateImpulse(src, rcv);
	//	std::copy(state->spatializerdata->sourcematrix, state->spatializerdata->sourcematrix + 16,
	//		prvsrcmat);
	//	std::copy(state->spatializerdata->listenermatrix, state->spatializerdata->listenermatrix + 16,
	//		prvrcvmat);
	//}

	//if (updateme) {
	//	updateme = false;
	//	float src[3];
	//	float rcv[3];
	//	GetCoordinates(state, src, rcv);
	//	CalculateImpulse(src, rcv);
	//}

	// Lock here so channel impulses don't get changed while reading
	impulselock.lock();

	int wo; // set inside loop

	for (int i = 0; i < inchannels; i++)
	{
		Channel& c = channel;

		// feed new data to input buffer s
		float* s = c.s;
		const int mask = fftsize - 1;
		wo = writeoffset;
		for (int n = 0; n < hopsize; n++)
		{
			s[wo] = inbuffer[n * inchannels + i];
			wo = (wo + 1) & mask;
		}

		// calculate X=FFT(s)
		wo = writeoffset;
		UnityComplexNumber* x = c.x[bufferindex];
		for (int n = 0; n < fftsize; n++)
		{
			x[n].Set(s[wo], 0.0f);
			wo = (wo + 1) & mask;
		}
		FFT::Forward(x, fftsize, false);

		wo = (wo + hopsize) & mask;

		// calculate y=IFFT(sum(convolve(H_k, X_k), k=1..numpartitions))
		UnityComplexNumber* y = tmpoutput;
		memset(y, 0, sizeof(UnityComplexNumber) * fftsize);
		for (int k = 0; k < numpartitions; k++)
		{
			UnityComplexNumber* h = c.h[k];
			UnityComplexNumber* x = c.x[(k + bufferindex) % numpartitions];
			for (int n = 0; n < fftsize; n++)
				UnityComplexNumber::MulAdd(h[n], x[n], y[n], y[n]);
		}
		FFT::Backward(y, fftsize, false);

		// overlap-save readout
		for (int n = 0; n < hopsize; n++)
		{
			float input = inbuffer[n * outchannels + i];
			outbuffer[n * outchannels + i] = input + (gain * y[n].re - input) * wet;
		}
	}

	impulselock.unlock();

	if (--bufferindex < 0)
		bufferindex = numpartitions - 1;

	writeoffset = wo;
}

int main()
{
	//std::cout << "starting" << std::endl;
	//RayTraceReverb* rtr = new RayTraceReverb();
	//rtr->RayTrace({ 0.0, 0.0, 0.0 }, { 0.0, -8.0, 0.0 });
	//delete rtr;
	//std::cout << "boom, done" << std::endl;
	//std::getchar();
	return 0;
}