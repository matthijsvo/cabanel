#pragma once

#include <memory>
#include "AudioPluginUtil.h"
#include "GeometryPool.h"

class GeometryPlugin
{
protected:
	std::weak_ptr<GeometryPool> geometry;
public:
	virtual void ConnectGeometry(const std::shared_ptr<GeometryPool>& geo)
	{
		geometry = geo;
	};
	virtual ~GeometryPlugin() {};
};