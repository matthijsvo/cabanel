#include "NullReverb.h"

void NullReverb::ProcessCallback(UnityAudioEffectState* state, float* inbuffer, float* outbuffer, unsigned int length, int inchannels, int outchannels)
{
	for (unsigned int n = 0; n < length; n++)
	{
		for (int i = 0; i < outchannels; i++)
		{
			outbuffer[n * outchannels + i] =
				inbuffer[n * outchannels + i];
		}
	}
};