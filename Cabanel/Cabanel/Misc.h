#pragma once

#include <math.h>
#include <iostream>

struct float3 {
	float3();
	float3(float xx, float yy, float zz);
	float3(const float *p);

	float3 operator-(const float3 &f2) const;
	float3 operator-() const;
	float3 operator*(float f) const;
	float3 operator*(const float3 &f2) const;
	float3 operator+(const float3 &f2) const;
	float3 &operator+=(const float3 &f2);
	float3 &operator*=(const float3 &f2);
	float3 &operator*=(const float &f2);
	float3 operator/(const float3 &f2) const;
	float3 operator/(const float &f2) const;
	float operator[](int i) const;
	float &operator[](int i);

	float3 neg();
	float length();
	void normalize();

	void copyFrom(const float* vec);
	void copyTo(float* vec);

	friend std::ostream& operator<<(std::ostream& os, const float3& obj);

	float x, y, z;
};

inline float3 normalize(float3 v)
{
	v.normalize();
	return v;
}

inline float3 operator*(float f, const float3 &v)
{
	return float3(v.x * f, v.y * f, v.z * f);
}

inline float3 vcross(float3 a, float3 b)
{
	float3 c;
	c[0] = a[1] * b[2] - a[2] * b[1];
	c[1] = a[2] * b[0] - a[0] * b[2];
	c[2] = a[0] * b[1] - a[1] * b[0];
	return c;
}

inline float vdot(float3 a, float3 b)
{
	return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}