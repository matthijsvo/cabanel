#include "ImageSourceReverb.h"


BoxParameters::BoxParameters()
{
	duration = defaultDuration;
	maxreflorder = defaultReflectionOrder;
	std::copy(defaultBoxPosition, defaultBoxPosition + 3, position);
	std::copy(defaultBoxDimensions, defaultBoxDimensions + 3, dimensions);
	std::copy(defaultBoxReflections, defaultBoxReflections + 6, reflections);
}


ImageSourceReverb::ImageSourceReverb(UnityAudioEffectState* state, int blocksize, int spr) 
{
	bufferindex = 0;
	writeoffset = 0;
	hopsize = blocksize;
	fftsize = blocksize * 2;
	tmpoutput = new UnityComplexNumber[fftsize];
	channels = new Channel[2];
	gotImport = false;

	box = new BoxParameters;

	// Calculate amount of partitions and samples
	numpartitions = 0;
	while (numpartitions * hopsize < box->duration * state->samplerate)
		numpartitions++;
	samples = numpartitions * hopsize;

	SetupImpulse(state, blocksize);
}


ImageSourceReverb::~ImageSourceReverb() 
{
	delete tmpoutput;
	delete channels;
	delete box;
}


void ImageSourceReverb::NotifyChanges() 
{
	gotImport = true;
}


inline float ImageSourceReverb::sinc(float x)
{
	return (x == 0) ? 1. : sin(x) / x;
}


inline float ImageSourceReverb::ToSampleDistance(float meters, int samplerate)
{
	return meters / (sound / (float)samplerate);
}


/**
* GetCoordinates
* Calculates relative coordinates of audio source and audio listener/receiver within the shoebox
* and inserts them in respectively given 'src' and 'rcv' arrays. These are expected to be 3 floats long.
*/
void ImageSourceReverb::GetCoordinates(float* src, float* rcv, UnityAudioEffectState* state)
{
	float* srcM = state->spatializerdata->sourcematrix;
	float* rcvM = state->spatializerdata->listenermatrix;
	float offsetX = box->position[0] - abs(box->dimensions[0] / 2);
	float offsetY = box->position[1] - abs(box->dimensions[1] / 2);
	float offsetZ = box->position[2] - abs(box->dimensions[2] / 2);
	src[0] = srcM[12] - offsetX;
	src[1] = srcM[13] - offsetY;
	src[2] = srcM[14] - offsetZ;
	rcv[0] = -(rcvM[0] * rcvM[12] + rcvM[1] * rcvM[13] + rcvM[2] * rcvM[14]) - offsetX;
	rcv[1] = -(rcvM[4] * rcvM[12] + rcvM[5] * rcvM[13] + rcvM[6] * rcvM[14]) - offsetY;
	rcv[2] = -(rcvM[8] * rcvM[12] + rcvM[9] * rcvM[13] + rcvM[10] * rcvM[14]) - offsetZ;
}


/**
* SetupImpulse
* Starts recalculation of room impulse response
*/
void ImageSourceReverb::SetupImpulse(UnityAudioEffectState* state, int length) 
{
	//MutexScopeLock mutexScope1(*data->mutex);
	float rcv[3];
	float src[3];

	// Get current source and listener coordinates from spatializer
	GetCoordinates(src, rcv, state);
		
	// Calculate impulse response
	SROOM(src, rcv, state->samplerate);

	// Partition the impulse response in preparation of convolution
	for (int ch = 0; ch < 2; ++ch) {
		Channel& c = channels[ch];
		c.h = new UnityComplexNumber*[numpartitions];
		c.x = new UnityComplexNumber*[numpartitions];
		float* src = c.impulse;
		for (int k = 0; k < numpartitions; k++)
		{
			c.h[k] = new UnityComplexNumber[fftsize];
			c.x[k] = new UnityComplexNumber[fftsize];
			memset(c.x[k], 0, sizeof(UnityComplexNumber) * fftsize);
			memset(c.h[k], 0, sizeof(UnityComplexNumber) * fftsize);
			for (int n = 0; n < hopsize; n++)
				c.h[k][n].re = *src++;
			FFT::Forward(c.h[k], fftsize, false);
		}
	}
}


/**
* SROOM
* Calculates room impulse response
*/
void ImageSourceReverb::SROOM(float* src, float* rcv, int samplerate) 
{
	float* rl = box->dimensions;
	float* beta = box->reflections;
	int order = box->maxreflorder;
	const int lpw = 2 * ceil(0.004*samplerate);
	float* lpi = new float[lpw];
	float rp[3];
	float r2l[3];

	// Convert distances from meters to sample distance
	float* s = new float[3];
	float* r = new float[3];
	float* l = new float[3];
	for (int i = 0; i < 3; ++i) {
		s[i] = src[i] / (sound / samplerate);
		r[i] = rcv[i] / (sound / samplerate);
		l[i] = box->dimensions[i] / (sound / samplerate);
	}

	for (int ch = 0; ch < 2; ++ch) {
		Channel& c = channels[ch];

		// New impulse array, initialized as zeroes
		c.impulse = new float[samples];
		memset(c.impulse, 0, sizeof(float) * samples);

		// Prepare for FFT later
		c.s = new float[fftsize];
		memset(c.s, 0, sizeof(float) * fftsize);

		if (src[0] == rcv[0] && src[1] == rcv[1] && src[2] == src[2]) {
			c.impulse[0] = 1.0;
			continue;
		}

		int n1 = (int)ceil(samples / (2 * l[0]));
		int n2 = (int)ceil(samples / (2 * l[1]));
		int n3 = (int)ceil(samples / (2 * l[2]));

		// Generate room impulse response
		for (int nx = -n1; nx <= n1; ++nx) {
			for (int ny = -n2; ny <= n2; ++ny) {
				for (int nz = -n3; nz <= n3; ++nz) {
					rp[0] = 2 * nx*l[0];
					rp[1] = 2 * ny*l[1];
					rp[2] = 2 * nz*l[2];

					for (int l = 0; l <= 1; ++l) {
						for (int j = 0; j <= 1; ++j) {
							for (int k = 0; k <= 1; ++k) {
								r2l[0] = (1 - 2 * l)*s[0] - r[0] + rp[0];
								r2l[1] = (1 - 2 * j)*s[1] - r[1] + rp[1];
								r2l[2] = (1 - 2 * k)*s[2] - r[2] + rp[2];

								float dist = sqrt(pow(r2l[0], 2) + pow(r2l[1], 2) + pow(r2l[2], 2));

								if (abs(2 * nx - l) + abs(2 * ny - j) + abs(2 * nz - k) <= box->maxreflorder || box->maxreflorder == -1) {
									float fdist = floor(dist);
									if (fdist < samples) {
										float delsq = pow(beta[0], abs(nx - l)) * pow(beta[1], abs(nx)) \
											* pow(beta[2], abs(ny - j)) * pow(beta[3], abs(ny)) \
											* pow(beta[4], abs(nz - k)) * pow(beta[5], abs(nz));

										float gain = delsq / (4 * M_PI * dist * (sound / samplerate));

										for (int n = 0; n < lpw; ++n) {
											lpi[n] = 0.5 * (1 - cos(2 * M_PI*((n + 1 - (dist - fdist)) / lpw))) * sinc(M_PI*(n + 1 - (dist - fdist) - (lpw / 2)));
										}

										int startPosition = (int)fdist - (lpw / 2) + 1;
										for (int n = 0; n < lpw; ++n) {
											if (startPosition + n >= 0 && startPosition + n < samples)
												c.impulse[startPosition + n] += gain * lpi[n];
										}
									}
								}
							}
						}
					}
				}
			}
		}

		const float W = 2 * 4 * M_PI * (samplerate / 100); // The cut-off frequency equals 100 Hz
		const float R1 = exp(-W);
		const float B1 = 2 * R1*cos(W);
		const float B2 = -R1 * R1;
		const float A1 = -(1 + R1);
		float       X0;
		float*      Y = new float[3];

		for (int idx = 0; idx < 3; idx++) { Y[idx] = 0; }
		for (int idx = 0; idx < samples; idx++)
		{
			X0 = c.impulse[idx];
			Y[2] = Y[1];
			Y[1] = Y[0];
			Y[0] = B1 * Y[1] + B2 * Y[2] + X0;
			c.impulse[idx] = Y[0] + A1 * Y[1] + R1 * Y[2];
		}

		std::ofstream logfile("cabanel_rir.log");
		if (logfile.is_open())
		{
			for (int i = 0; i < samples; ++i) {
				logfile << i << " " << c.impulse[i] << "\n";
			}
		}
	}
}


void ImageSourceReverb::UpdateImpulse(UnityAudioEffectState* state)
{
	gotImport = true;
	// TODO perform update here and on different thread
}


void ImageSourceReverb::ProcessCallback(UnityAudioEffectState* state, float* inbuffer, float* outbuffer, unsigned int length, int inchannels, int outchannels)
{
	const float wet = 30.0f * 0.01f;
	const float gain = powf(10.0f, 0.05f);

	if (gotImport) {
		// Recalculate samples, partitions
		numpartitions = 0;
		while (numpartitions * hopsize < box->duration * state->samplerate)
			numpartitions++;
		samples = numpartitions * hopsize;

		SetupImpulse(state, length);
		std::copy(state->spatializerdata->sourcematrix, state->spatializerdata->sourcematrix + 16,
			prvsrcmat);
		std::copy(state->spatializerdata->listenermatrix, state->spatializerdata->listenermatrix + 16,
			prvrcvmat);
		gotImport = false;
	}

	// Lock data here in case float parameters are changed in pause/stopped mode and cause further calls to SetupImpulse
	//MutexScopeLock mutexScope1(*mutex);

	int wo; // set inside loop

	for (int i = 0; i < inchannels; i++)
	{
		Channel& c = channels[i];

		// feed new data to input buffer s
		float* s = c.s;
		const int mask = fftsize - 1;
		wo = writeoffset;
		for (int n = 0; n < hopsize; n++)
		{
			s[wo] = inbuffer[n * inchannels + i];
			wo = (wo + 1) & mask;
		}

		// calculate X=FFT(s)
		wo = writeoffset;
		UnityComplexNumber* x = c.x[bufferindex];
		for (int n = 0; n < fftsize; n++)
		{
			x[n].Set(s[wo], 0.0f);
			wo = (wo + 1) & mask;
		}
		FFT::Forward(x, fftsize, false);

		wo = (wo + hopsize) & mask;

		// calculate y=IFFT(sum(convolve(H_k, X_k), k=1..numpartitions))
		UnityComplexNumber* y = tmpoutput;
		memset(y, 0, sizeof(UnityComplexNumber) * fftsize);
		for (int k = 0; k < numpartitions; k++)
		{
			UnityComplexNumber* h = c.h[k];
			UnityComplexNumber* x = c.x[(k + bufferindex) % numpartitions];
			for (int n = 0; n < fftsize; n++)
				UnityComplexNumber::MulAdd(h[n], x[n], y[n], y[n]);
		}
		FFT::Backward(y, fftsize, false);

		// overlap-save readout
		for (int n = 0; n < hopsize; n++)
		{
			float input = inbuffer[n * outchannels + i];
			outbuffer[n * outchannels + i] = input + (gain * y[n].re - input) * wet;
		}
	}

	if (--bufferindex < 0)
		bufferindex = numpartitions - 1;

	writeoffset = wo;
}
