#pragma once

#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <windows.h>

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <random>
#include <future>
#include <vector>
#include <exception>
#include "GeometryPool.h"
#include "AudioPluginUtil.h"
#include "SpatializerSubPlugin.h"
#include "GeometryPlugin.h"
#include "Misc.h"

//
// NULL REVERB
// Does absolutely nothing, sends audio right back
// Intended for test purposes mainly
//
class NullReverb : public SpatializerSubPlugin, public GeometryPlugin
{
public:
	NullReverb() {};
	~NullReverb() {};

	// SPATIALIZERSUBPLUGIN INTERFACE:
public:
	void UpdateImpulse(UnityAudioEffectState* state) {};
	void ProcessCallback(UnityAudioEffectState* state, float* inbuffer, float* outbuffer, unsigned int length, int inchannels, int outchannels);
};