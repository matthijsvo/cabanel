// Please note that this will only work on Unity 5.2 or higher.

#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <windows.h>

#include "AudioPluginUtil.h"
#include "Plugin_CabanelSpatializer.h"

#define DLLExport __declspec(dllexport)

namespace Spatializer
{
	//
	// CABANEL C# INTERFACE
	//
	extern "C"
	{
		DLLExport int Reset()
		{
			State::GetInstance().started = false;
			State::GetInstance().mainData->subplugins.clear();
			State::GetInstance().mainData->pluginmap.clear();
			State::GetInstance().mainData->geometrypools.clear();
			return 0;
		}

		// Run when everything's properly configured
		DLLExport int StartSpatializer()
		{
			State::GetInstance().started = true;
			return 0;
		}

		DLLExport int StopSpatializer()
		{
			State::GetInstance().started = false;
			return 0;
		}

		DLLExport int AddImageSourceReverb(const char* pluginid)
		{
			State& state = State::GetInstance();
			AddSubPlugin(pluginid, std::make_shared<ImageSourceReverb>(state.audioState, 1024, state.audioState->samplerate));
			return 0;
		}


		DLLExport int AddRayTraceReverb(const char* pluginid)
		{
			AddSubPlugin(pluginid, std::make_shared<RayTraceReverb>(State::GetInstance().audioState, 1024));
			return 0;
		}


		DLLExport int AddMinTraceReverb(const char* pluginid, int n)
		{
			AddSubPlugin(pluginid, std::make_shared<MinTraceReverb>(State::GetInstance().audioState, 1024, n));
			return 0;
		}


		DLLExport int AddNullReverb(const char* pluginid)
		{
			AddSubPlugin(pluginid, std::make_shared<NullReverb>());
			return 0;
		}


		DLLExport int RemovePlugin(const char* pluginid)
		{
			RemoveSubPlugin(pluginid);
			return 0;
		}


		DLLExport int AddGeometryPool(const char* pluginid)
		{
			State::GetInstance().mainData->geometrypools[pluginid] = std::make_shared<GeometryPool>();
			return 0;
		}


		/** RemoveGeometryPool
		 * IMPORTANT: Make sure you reconnect any active plugins dependent on this geometry pool to a different one first!
		 */
		DLLExport int RemoveGeometryPool(const char* pluginid)
		{
			if (State::GetInstance().mainData->geometrypools[pluginid])
			{
				State::GetInstance().mainData->geometrypools.erase(pluginid);
			}
			else
			{
				throw std::runtime_error("Nonexistent geometry pool ID!");
			}
			return 0;
		}


		DLLExport int ImportGeometry(const char* geoid, float* vertices, int verts, int* triangles, int tris, float coef)
		{
			if (auto& gp = State::GetInstance().mainData->geometrypools[geoid])
			{
				gp->Import(vertices, verts, triangles, tris, coef);
			}
			else
			{
				throw std::runtime_error("Nonexistent geometry pool ID!");
			}
			return 0;
		}


		DLLExport int ClearGeometryPool(const char* geoid)
		{
			if (auto& gp = State::GetInstance().mainData->geometrypools[geoid])
			{
				gp->Clear();
			}
			else
			{
				throw std::runtime_error("Nonexistent geometry pool ID!");
			}
			return 0;
		}


		DLLExport int ConnectGeometryPool(const char* pluginid, const char* geometryid)
		{
			if (GeometryPlugin* plugin = dynamic_cast<GeometryPlugin*>(State::GetInstance().mainData->pluginmap[pluginid].lock().get()))
			{
				if (auto& gp = State::GetInstance().mainData->geometrypools[geometryid])
				{
					plugin->ConnectGeometry(gp);
				}
				else
				{
					throw std::runtime_error("Nonexistent geometry pool ID!");
				}
			}
			else
			{
				throw std::runtime_error("Cannot connect geometry to inexistent or incompatible plugin!");
			}
			return 0;
		}


		DLLExport int UpdateImpulse(const char* pluginid)
		{
			if (auto plugin = State::GetInstance().mainData->pluginmap[pluginid].lock())
			{
				plugin->UpdateImpulse(State::GetInstance().audioState);
			}
			else
			{
				throw std::runtime_error("Nonexistent subplugin ID!");
			}
			return 0;
		}


		DLLExport int ToggleActive(const char* pluginid)
		{
			if (auto plugin = State::GetInstance().mainData->pluginmap[pluginid].lock())
			{
				plugin->ToggleActive();
			}
			else
			{
				throw std::runtime_error("Nonexistent subplugin ID!");
			}
			return 0;
		}


		DLLExport int SetActive(const char* pluginid, bool active)
		{
			if (auto plugin = State::GetInstance().mainData->pluginmap[pluginid].lock())
			{
				plugin->SetActive(active);
			}
			else
			{
				throw std::runtime_error("Nonexistent subplugin ID!");
			}
			return 0;
		}


		DLLExport int UpdateImageSourceSettings(const char* pluginid, 
			float posX, float posY, float posZ,
			float dimX, float dimY, float dimZ,
			float b0, float b1, float b2, float b3, float b4, float b5,
			int delsq, float dur)
		{
			if (ImageSourceReverb* plugin = dynamic_cast<ImageSourceReverb*>(State::GetInstance().mainData->pluginmap[pluginid].lock().get()))
			{
				BoxParameters* box = plugin->box;
				box->position[0] = posX;
				box->position[1] = posY;
				box->position[2] = posZ;
				box->dimensions[0] = dimX;
				box->dimensions[1] = dimY;
				box->dimensions[2] = dimZ;
				box->reflections[0] = b0;
				box->reflections[1] = b1;
				box->reflections[2] = b2;
				box->reflections[3] = b3;
				box->reflections[4] = b4;
				box->reflections[5] = b5;
				box->duration = dur;
			}
			else
			{
				throw std::runtime_error("Inexistent or incompatible plugin!");
			}
			return 0;
		}

		DLLExport int UpdateRayTraceSettings(const char* pluginid, float rad, int maxrefl, int rays, float dur)
		{
			if (RayTraceReverb* plugin = dynamic_cast<RayTraceReverb*>(State::GetInstance().mainData->pluginmap[pluginid].lock().get()))
			{
				RayTraceSettings* rts = plugin->settings;
				rts->listenerradius = rad;
				rts->maxreflections = maxrefl;
				rts->rays = rays;
				rts->duration = dur;
			}
			else
			{
				throw std::runtime_error("Inexistent or incompatible plugin!");
			}
			return 0;
		}

		DLLExport int ExportImpulsePlot(const char* pluginid, const char* filename, int xres, int yres)
		{
			if (RayTraceReverb* plugin = dynamic_cast<RayTraceReverb*>(State::GetInstance().mainData->pluginmap[pluginid].lock().get()))
			{
				try
				{
					std::vector<float> plotvec(plugin->channel.impulse, plugin->channel.impulse + plugin->samples);
					matplotlibcpp::figure_size(xres, yres);
					matplotlibcpp::plot(plotvec);
					matplotlibcpp::save(filename);
				}
				catch (std::runtime_error re)
				{
					std::cerr << "[ERROR] Exporting plot failed!" << std::endl;
				}
			}
			else
			{
				throw std::runtime_error("Inexistent or incompatible plugin!");
			}
			return 0;
		}
	}


    int InternalRegisterEffectDefinition(UnityAudioEffectDefinition& definition)
    {
		int numparams = P_NUM;
		definition.paramdefs = new UnityAudioParameterDefinition[numparams];
		RegisterParameter(definition, "AudioSrc Attn", "", 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, P_AUDIOSRCATTN, "AudioSource distance attenuation");
		RegisterParameter(definition, "Fixed Volume", "", 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, P_FIXEDVOLUME, "Fixed volume amount");
		RegisterParameter(definition, "Custom Falloff", "", 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, P_CUSTOMFALLOFF, "Custom volume falloff amount (logarithmic)");
		RegisterParameter(definition, "Frequency", "Hz", 0.0f, kMaxSampleRate, 1000.0f, 1.0f, 3.0f, P_FREQ, "Frequency of sine oscillator that is multiplied with the input signal");
		RegisterParameter(definition, "Mix amount", "", 0.0f, 1.0f, 0.5f, 1.0f, 1.0f, P_MIX, "Ratio between input and ring-modulated signals");
		definition.flags |= UnityAudioEffectDefinitionFlags_IsSpatializer;
		return numparams;
    }


	inline bool IsHostCompatible(UnityAudioEffectState* state)
	{
		// Somewhat convoluted error checking here because hostapiversion is only supported from SDK version 1.03 (i.e. Unity 5.2) and onwards.
		return
			state->structsize >= sizeof(UnityAudioEffectState) &&
			state->hostapiversion >= UNITY_AUDIO_PLUGIN_API_VERSION;
	}


	void AddSubPlugin(std::string id, const std::shared_ptr<SpatializerSubPlugin>& plugin)
	{
		State* state = &State::GetInstance();
		state->mainData->subplugins.push_back(plugin);
		state->mainData->pluginmap.insert(std::make_pair(id, plugin));
	}


	void RemoveSubPlugin(std::string id)
	{
		State* state = &State::GetInstance();
		if (auto plugin = state->mainData->pluginmap[id].lock())
		{
			state->mainData->subplugins.erase(std::remove(state->mainData->subplugins.begin(), state->mainData->subplugins.end(), plugin), state->mainData->subplugins.end());
			state->mainData->pluginmap.erase(id);
		}
		else
		{
			throw std::runtime_error("Nonexistent subplugin ID: " + id);
		}
	}


	//
	// IMPLEMENT CUSTOM DISTANCE ATTENUATION HERE
	//
	UNITY_AUDIODSP_RESULT UNITY_AUDIODSP_CALLBACK DistanceAttenuationCallback(UnityAudioEffectState* state, float distanceIn, float attenuationIn, float* attenuationOut)
	{
		EffectData* data = state->GetEffectData<EffectData>();
		*attenuationOut =
			data->p[P_AUDIOSRCATTN] * attenuationIn +
			data->p[P_FIXEDVOLUME] +
			data->p[P_CUSTOMFALLOFF] * (1.0f / FastMax(1.0f, distanceIn));
		return UNITY_AUDIODSP_OK;
	}


    UNITY_AUDIODSP_RESULT UNITY_AUDIODSP_CALLBACK CreateCallback(UnityAudioEffectState* state)
    {
		EffectData* effectdata = new EffectData;
		state->effectdata = effectdata;

		HRTFConvolution::Setup(state, &effectdata->hrtfdata);

		effectdata->c = 1.0f;
		
		if (IsHostCompatible(state))
			state->spatializerdata->distanceattenuationcallback = DistanceAttenuationCallback;
		InitParametersFromDefinitions(InternalRegisterEffectDefinition, effectdata->p);

		State* spatState = &State::GetInstance();
		spatState->audioState = state;
		spatState->mainData = effectdata;

        return UNITY_AUDIODSP_OK;
    }


    UNITY_AUDIODSP_RESULT UNITY_AUDIODSP_CALLBACK ReleaseCallback(UnityAudioEffectState* state)
    {
		EffectData* data = state->GetEffectData<EffectData>();
		data->subplugins.clear();
		data->pluginmap.clear();
		data->geometrypools.clear();

		// Fun fact: Unity's memory management is insane.
		State::GetInstance().started = false;
        
		return UNITY_AUDIODSP_OK;
    }


    UNITY_AUDIODSP_RESULT UNITY_AUDIODSP_CALLBACK SetFloatParameterCallback(UnityAudioEffectState* state, int index, float value)
    {
        return UNITY_AUDIODSP_OK;
    }


    UNITY_AUDIODSP_RESULT UNITY_AUDIODSP_CALLBACK GetFloatParameterCallback(UnityAudioEffectState* state, int index, float* value, char *valuestr)
    {
        return UNITY_AUDIODSP_OK;
    }


    int UNITY_AUDIODSP_CALLBACK GetFloatBufferCallback(UnityAudioEffectState* state, const char* name, float* buffer, int numsamples)
    {
        return UNITY_AUDIODSP_OK;
    }


    UNITY_AUDIODSP_RESULT UNITY_AUDIODSP_CALLBACK ProcessCallback(UnityAudioEffectState* state, float* inbuffer, float* outbuffer, unsigned int length, int inchannels, int outchannels)
    {
		if (!State::GetInstance().started) return UNITY_AUDIODSP_OK;

		State::GetInstance().audioState = state;

		EffectData* data = state->GetEffectData<EffectData>();
		for (int i = 0; i < data->subplugins.size(); ++i)
		{
			if (data->subplugins[i]->IsActive())
			{
				data->subplugins[i]->ProcessCallback(state, inbuffer, outbuffer, length, inchannels, outchannels);
				inbuffer = outbuffer;
			}
		}
		HRTFConvolution::ProcessCallback(state, &data->hrtfdata, inbuffer, outbuffer, length, inchannels, outchannels);
        return UNITY_AUDIODSP_OK;
    }
}
