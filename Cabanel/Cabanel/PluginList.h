//DECLARE_EFFECT("Cabanel RingModulator", RingModulator)
//DECLARE_EFFECT("Cabanel ConvolutionReverb", ConvolutionReverb)

#if !UNITY_PS3
DECLARE_EFFECT("Cabanel Spatializer", Spatializer)
DECLARE_EFFECT("Cabanel Velvet Reverb", SpatializerReverb)
#endif
