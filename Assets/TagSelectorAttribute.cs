using UnityEngine;
 
//Original version of the TagSelectorAttribute created by Brecht Lecluyse (www.brechtos.com)
//Modified by: -
 
public class TagSelectorAttribute : PropertyAttribute
{
    public bool UseDefaultTagFieldDrawer = false;
}