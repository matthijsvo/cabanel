﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class CabanelGeometryConnector : MonoBehaviour
{
    // If this script is set to standalone it will on startup import all geometry with the tag <geometryTag> to geometry pool <GeometryPoolID>
    // Else this script will do nothing until commanded by another script
    [Tooltip("If active: automatically import all geometry with the given tag to the given geometry pool \nElse: do nothing until commanded by another script")]
    public bool standalone = true;

    // ID string of the geometry pool to import to
    [ConditionalHide("standalone", true)]
    [Tooltip("ID string of the geometry pool to import to")]
    public string geometryPoolID = "geo1";

    // Set to true if geometry pool already exists, set to false if a new one needs to be created first
    // Useful when importing multiple tags into same geometry pool
    // Ignored if not standalone!
    [ConditionalHide("standalone", true)]
    [Tooltip("Set to true if geometry pool already exists, set to false if a new one needs to be created first")]
    public bool existingPool = true;

    // Tag of all geometry to be imported to the geometry pool
    [Tooltip("Tag of all geometry to be imported to the geometry pool")]
    [TagSelector]
    public string geometryTag = "";

    // Default reflection coefficient in case a geometry object with tag <geometryTag> doesn't carry a CabanelGeometryAttributes script with its own value
    [Tooltip("Default reflection coefficient for all tagged objects without attached CabanelGeometryAttributes script")]
    public float defaultReflectionCoefficient = 0.8f;


    [DllImport("AudioPluginCabanel", CallingConvention = CallingConvention.Cdecl)]
    private static extern int AddGeometryPool(string pluginid);

    [DllImport("AudioPluginCabanel")]
    private static extern int ImportGeometry(string geoid, float[] vertices, int verts, int[] triangles, int tris, float coef);

    [DllImport("AudioPluginCabanel")]
    private static extern int ClearGeometryPool(string geoid);


    void Awake()
    {
        if (standalone)
        {
            if(!existingPool)
                CreateGeometryPool();
            ImportGeometryWithTag();
        }
        else {

        }
        //Debug.Log("Sending geometry to Cabanel...");
        //Mesh mesh = GetComponent<MeshFilter>().mesh;
        //Vector3[] vertices = mesh.vertices;
        //int verts = vertices.Length * 3;
        //float[] vertArray = new float[verts];
        //int[] triangles = mesh.triangles;
        //Vector3 scale = transform.localScale;
        //for (int i = 0; i < vertices.Length; ++i)
        //{
        //    vertArray[3 * i] = vertices[i][0] * scale[0];
        //    vertArray[3 * i + 1] = vertices[i][1] * scale[1];
        //    vertArray[3 * i + 2] = vertices[i][2] * scale[2];
        //}

        //ImportGeometry(vertArray, verts, triangles, triangles.Length, reflectionCoefficient);

        ////Debug.Log("Done.");
    }

    // Update is called once per frame
    void Update()
    {
        // Nuffin' here, boss
    }

    public void CreateGeometryPool()
    {
        AddGeometryPool(geometryPoolID);
    }

    public void ImportGeometryWithTag()
    {
        // First clear all old geometry from ray tracer
        ClearGeometryPool(geometryPoolID);

        GameObject[] objects;
        objects = GameObject.FindGameObjectsWithTag(geometryTag);

        for (int o = 0; o < objects.Length; o++)
        {
            GameObject thingie = objects[o];
            // Get geometry from object
            Mesh mesh = thingie.GetComponent<MeshFilter>().mesh;
            Vector3[] vertices = mesh.vertices;
            int verts = vertices.Length * 3;
            float[] vertArray = new float[verts];
            int[] triangles = mesh.triangles;
            Vector3 scale = thingie.transform.localScale;
            for (int i = 0; i < vertices.Length; ++i)
            {
                vertArray[3 * i] = vertices[i][0] * scale[0];
                vertArray[3 * i + 1] = vertices[i][1] * scale[1];
                vertArray[3 * i + 2] = vertices[i][2] * scale[2];
            }

            // Get other attributes, if present
            float reflectionCoefficient = defaultReflectionCoefficient;
            CabanelGeometryAttributes attrs = thingie.GetComponent<CabanelGeometryAttributes>();
            if (attrs != null)
            {
                reflectionCoefficient = attrs.reflectionCoefficient;
            }

            ImportGeometry(geometryPoolID, vertArray, verts, triangles, triangles.Length, reflectionCoefficient);
        }

    }
}
