using System;
using System.Collections;
using System.Collections.Generic;

// COMMON INTERFACE FOR PLUGIN SETTINGS SCRIPTS
// DO NOT USE DIRECTLY IN UNITY
public interface CabanelPluginSettings
{
    void UpdateSettings();
    void SetPluginID(string id);
}
