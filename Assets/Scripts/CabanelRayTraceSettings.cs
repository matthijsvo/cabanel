﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;


public class CabanelRayTraceSettings : MonoBehaviour, CabanelPluginSettings
{
    [DllImport("AudioPluginCabanel", CallingConvention = CallingConvention.Cdecl)]
    private static extern int UpdateRayTraceSettings(string pluginid, float rad, int maxrefl, int rays, float dur);

    public bool standalone = true;

    [ConditionalHide("standalone", true)]
    [Tooltip("Cabanel SubPlugin ID of the ray tracer to be configured")]
    public string rayTraceID = "raytracer";

    public float listenerRadius = -1.0f;
    private float _lr;

    public int maxReflections = 8;
    private int _mr;

    public int rays = 1000;
    private int _ra;

    public float duration = 1.0f;
    private float _du;

    // Use this for initialization
    void Start()
    {
        SetPrevs();
        if (standalone)
        {
            UpdateSettings();
        }
    }

    // Update is called once per frame
    void Update()
    {
        // there is no more elegant way to act on changes unfortunately
        if (listenerRadius != _lr || maxReflections != _mr || rays != _ra || duration != _du)
        {
            UpdateRayTraceSettings(rayTraceID, listenerRadius, maxReflections, rays, duration);
            SetPrevs();
        }
    }

    public void SetPrevs()
    {
        _lr = listenerRadius;
        _mr = maxReflections;
        _ra = rays;
        _du = duration;
    }

    public void UpdateSettings()
    {
        UpdateRayTraceSettings(rayTraceID, listenerRadius, maxReflections, rays, duration);
    }

    public void SetPluginID(string id)
    {
        rayTraceID = id;
    }
}
