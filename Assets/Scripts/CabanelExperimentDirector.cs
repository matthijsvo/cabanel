﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class CabanelExperimentDirector : MonoBehaviour
{
    [ReadOnly]
    public string state = State.STOPPED.ToString("g");

    [Header("Links")]
    // The game object containing the player and an Audio Listener script
    [Tooltip("Game object containing the player and an Audio Listener script")]
    public GameObject listener;

    // The game object containing the sound source, including an Audio Source script
    [Tooltip("Game object containing the sound source, including an Audio Source script")]
    public GameObject source;

    // A game object (can be the listener) indicating the center of the sphere on which the sound source will be randomly placed
    [Tooltip("Center of sound sphere")]
    public GameObject centerObject;

    // The Cabanel Geometry Connector script which will import the scene geometry
    // Be sure to also configure the right geometry tag in the referenced script!
    [Tooltip("CabanelGeometryConnector script to be used (disable 'standalone'!)")]
    public CabanelGeometryConnector geometryConnector;

    // Script for Ray Tracer settings
    [Tooltip("CabanelRayTraceSettings script to be used (disable 'standalone'!)")]
    public CabanelRayTraceSettings rayTraceSettings;

    // Audio cue to be played, insert an Audio Clip here
    [Tooltip("Audio cue to be played")]
    public AudioClip cue;

    [Header("Tags")]
    // Tag of the game object serving as the calibration target
    
    [TagSelector]
    [Tooltip("Tag of any game object(s) serving as the calibration target")]
    public string calibrationTag = "";

    // Tag of the game object which, when pointed at during the test, indicates "no answer"
    [TagSelector]
    [Tooltip("Tag of any game object(s) which, when pointed at during the test, indicate 'no answer'")]
    public string noAnswerTag = "";

    [Header("Settings")]

    // Radius of the sphere on which audio sources will be randomly placed
    [Tooltip("Radius of the sphere on which audio sources will be randomly placed")]
    public float sphereRadius = 2.0f;

    // Delays the time (in seconds) before audio is played after succesful calibration
    // If the audio doesn't run properly, increase this value to give some extra time to the backend to calculate the impulse response
    [Tooltip("Delays the time (in seconds) before the audio cue is played after succesful calibration")]
    public float IRCalcTime = 0.5f;

    // Prefix of the filename to be exported when the experiment is done
    // Suggestion: use test subject name
    // Full filename will also contain date and time
    [Tooltip("Prefix of test results filename (e.g. test subject name), is appended by time and date")]
    public string exportFilename = "Experiment";
    

    private Vector3 center;
    
    // RNG
    private System.Random optrng;
    private System.Random phidis;
    private System.Random costhetadis;

    // Plugin IDs
    private string raytraceid;
    private string mintraceid;
    private string nulltraceid;
    private string geometryid = "geo1";


    private enum State
    {
        STOPPED,
        CALIBRATION,   // User needs to align crosshair with base position
        TEST           // Cue is played, user can aim
    };
    private State currentState;

    private enum Type
    {
        DRY,      // No reverb
        SHORT,    // Ray tracing with n-shortest (non-direct) path impulse only
        FULL      // Full ray tracing
    };
    private Type currentType;

    private struct Entry
    {
        public Type     testType;
        public Vector3  sourceVector;
        public Vector3  aimVector;
        public string   time;
        public bool     noAnswer;

        public Entry(Type _type, string _time, Vector3 _sourceVector)
        {
            testType = _type;
            time = _time;
            sourceVector = _sourceVector;
            aimVector = new Vector3();
            noAnswer = true;
        }

        public override string ToString()
        {
            return time + "\t"
                + testType.ToString("g") + "\t"
                + sourceVector.x + "\t"
                + sourceVector.y + "\t"
                + sourceVector.z + "\t"
                + aimVector.x + "\t"
                + aimVector.y + "\t"
                + aimVector.z + "\t"
                + noAnswer;
        }

        public static string GetHeader()
        {
            return "time" + "\t"
                + "testtype" + "\t"
                + "target x" + "\t"
                + "target y" + "\t"
                + "target z" + "\t"
                + "aim x" + "\t"
                + "aim y" + "\t"
                + "aim z" + "\t"
                + "no answer";
        }
    }
    private List<Entry> log;

    [DllImport("AudioPluginCabanel")]
    private static extern int StartSpatializer();

    [DllImport("AudioPluginCabanel", CallingConvention = CallingConvention.Cdecl)]
    private static extern int AddRayTraceReverb(string pluginid);

    [DllImport("AudioPluginCabanel", CallingConvention = CallingConvention.Cdecl)]
    private static extern int AddMinTraceReverb(string pluginid);

    [DllImport("AudioPluginCabanel", CallingConvention = CallingConvention.Cdecl)]
    private static extern int AddNullReverb(string pluginid);

    [DllImport("AudioPluginCabanel", CallingConvention = CallingConvention.Cdecl)]
    private static extern int ConnectGeometryPool(string pluginid, string geoid);

    [DllImport("AudioPluginCabanel", CallingConvention = CallingConvention.Cdecl)]
    private static extern int SetActive(string pluginid, bool active);

    [DllImport("AudioPluginCabanel", CallingConvention = CallingConvention.Cdecl)]
    private static extern int UpdateImpulse(string pluginid);

    [DllImport("AudioPluginCabanel", CallingConvention = CallingConvention.Cdecl)]
    private static extern int ExportImpulsePlot(string pluginid, string filename, int xres, int yres);


    void Awake()
    {
        
    }

    private void Start()
    {
        optrng = new System.Random();
        phidis = new System.Random();
        costhetadis = new System.Random();

        log = new List<Entry>();

        center = centerObject.transform.position;
        state = State.STOPPED.ToString("g");
        currentState = State.STOPPED;
        currentType = Type.FULL;

        source.GetComponent<AudioSource>().clip = cue;
        source.GetComponent<AudioSource>().loop = false;

        raytraceid = Type.FULL.ToString("g");
        //mintraceid = Type.SHORT.ToString("g");
        //nulltraceid = Type.DRY.ToString("g");

        AddRayTraceReverb(raytraceid);
        //AddMinTraceReverb(mintraceid);
        //AddNullReverb(nulltraceid);

        geometryConnector.standalone = false;
        geometryConnector.geometryPoolID = geometryid;
        geometryConnector.CreateGeometryPool();
        geometryConnector.ImportGeometryWithTag();

        rayTraceSettings.standalone = false;
        rayTraceSettings.SetPluginID(raytraceid);
        rayTraceSettings.UpdateSettings();

        ConnectGeometryPool(raytraceid, geometryid);
        //ConnectGeometryPool(mintraceid, geometryid);

        //PrepareImpulse();

        StartSpatializer();
    }

    void Update()
    {
        //if (Input.GetKeyUp("space"))
        //{
        //    currentState = State.STOPPED;
        //    state = State.STOPPED.ToString("g");
        //    ExportLog(exportFilename);
        //}

        if (Input.GetKeyUp("space"))
        {
            //StartCoroutine(PlayCue());
            source.GetComponent<AudioSource>().Play();
        }

        if (Input.GetKeyUp("e"))
        {
            Debug.Log("Starting impulse export.");
            ExportImpulsePlot(raytraceid, "./impulse_" + raytraceid + "_" + DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("HHmmss") + ".png", 1280, 720);
            Debug.Log("Done.");
        }

        if (Input.GetKeyUp("l"))
        {
            Debug.Log("Refreshing impulse.");
            UpdateImpulse(currentType.ToString("g"));
            Debug.Log("Done.");
        }

        //switch (currentState)
        //{
        //    case State.STOPPED:
        //        if (Input.GetButtonDown("Fire1"))
        //        {
        //            currentState = State.CALIBRATION;
        //            state = State.CALIBRATION.ToString("g");
        //        }
        //        break;
        //    case State.CALIBRATION:
        //        if (Input.GetButtonDown("Fire1"))
        //        {
        //            if (LookingAtCalibrationTarget())
        //            {
        //                StartCoroutine(PlayCue());
        //                currentState = State.TEST;
        //                state = State.TEST.ToString("g");
        //            }
        //            else
        //            {
        //                // Keep calibrating
        //            }
        //        }
        //        break;
        //    case State.TEST:
        //        if (Input.GetButtonDown("Fire1"))
        //        {
        //            StartCoroutine(PlayCue());
        //            //LogResult();
        //            //currentState = State.CALIBRATION;
        //            //state = State.CALIBRATION.ToString("g");
        //        }
        //        break;

            //}
    }


    void OnApplicationQuit()
    {
        //ExportLog(exportFilename);
    }


    /** LookingAtCalibrationTarget
     * Determines whether user is looking at calibration target
     */
    bool LookingAtCalibrationTarget()
    {
        RaycastHit hit;
        return Physics.Raycast(listener.transform.position, listener.transform.forward, out hit, 10) &&
            hit.collider.gameObject.CompareTag(calibrationTag);
    }


    /** PlaceSource
     * Moves audio source to random position on sphere (see 'sphereRadius') around center (see 'centerObject')
     */
    void PlaceSource()
    {
        double phi = phidis.NextDouble() * (2 * Math.PI);
        double theta = Math.Acos(costhetadis.NextDouble() * 2.0 - 1.0);

        // Random unit vector:
        Vector3 direction = new Vector3((float) (Math.Sin(theta) * Math.Cos(phi)), (float) (Math.Sin(theta) * Math.Sin(phi)), (float) Math.Cos(theta));

        source.transform.position = center + direction * sphereRadius;
    }


    /** PrepareImpulse
     * Places audio source on sphere and lets Cabanel calculate an Impulse Response
     */
    void PrepareImpulse()
    {
        //PlaceSource();
        UpdateImpulse(currentType.ToString("g"));
    }


    /** PlayCue
     * Optionally waits for 'IRCalcTime', then plays audio clip 'cue'
     * Creates new entry in log with start time
     * Starts computation for the next audio target directly after
     */
    IEnumerator PlayCue()
    {
        ConfigureReverbType();
        if (IRCalcTime > 0)
            yield return new WaitForSeconds(IRCalcTime);
        // Add new entry which later receives aiming information
        log.Add(new Entry(currentType, DateTime.Now.ToString("yyyyMMddHHmmss"), source.transform.position - listener.transform.position));
        source.GetComponent<AudioSource>().Play();

        // Already prepare for next cue here to not lose time

        currentType = (Type)optrng.Next(3);
        PrepareImpulse();
        yield break;
    }


    void ConfigureReverbType()
    {
        switch(currentType)
        {
            case Type.DRY:
                SetActive(raytraceid, false);
                SetActive(mintraceid, false);
                break;
            case Type.SHORT:
                SetActive(raytraceid, false);
                SetActive(mintraceid, true);
                break;
            case Type.FULL:
                SetActive(raytraceid, true);
                SetActive(mintraceid, false);
                break;
        }

    }


    /** LogResult
     * Logs the result for a single test run
     */
    void LogResult()
    {
        // Pick up the entry created when the audio was played (see PlayCue) to insert aiming data
        Entry entry = log[log.Count - 1];
        entry.aimVector = listener.transform.position + (listener.transform.forward * sphereRadius);

        // Check if aimed within "no answer zone"
        RaycastHit hit;
        Physics.Raycast(listener.transform.position, listener.transform.forward, out hit, 10);
        entry.noAnswer = hit.collider.gameObject.CompareTag(noAnswerTag);
    }


    /** ExportLog
     * Export log to file in current directory in a tab-separated format
     */
    void ExportLog(string filename)
    {
        System.IO.File.WriteAllText(
        AppDomain.CurrentDomain.BaseDirectory + @"\" + filename + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".log",
        Entry.GetHeader() + "\n" + log);
    }

}
