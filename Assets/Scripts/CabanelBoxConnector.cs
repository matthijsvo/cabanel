﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class CabanelBoxConnector : MonoBehaviour
{

    [DllImport("AudioPluginCabanel")]
    private static extern int ImportBoxParams(float posX, float posY, float posZ,
                        float dimX, float dimY, float dimZ,
                        float b0, float b1, float b2, float b3, float b4, float b5,
                        int refl, float dur);

    [DllImport("AudioPluginCabanel")]
    private static extern int ToggleImageSourceReverb();

    public bool active = true;
    private bool prevActive = true;
    public float duration = 0.5f;
    public int reflectionOrder = -1;
    public Vector2 xReflectiveness = new Vector2(1, 1);
    public Vector2 yReflectiveness = new Vector2(1, 1);
    public Vector2 zReflectiveness = new Vector2(1, 1);

    // Use this for initialization
    void Start()
    {
        Debug.Log("Posting shoebox coordinates to Cabanel...");
        Vector3 pos = transform.position;
        Vector3 scl = transform.localScale;

        ImportBoxParams(pos.x, pos.y, pos.z,
            scl.x, scl.y, scl.z, 
            xReflectiveness.x, xReflectiveness.y, yReflectiveness.x, yReflectiveness.y, zReflectiveness.x, zReflectiveness.y, 
            reflectionOrder, duration);

        Debug.Log("Done.");
    }

    // Update is called once per frame
    void Update()
    {
        if(active != prevActive)
        {
            ToggleImageSourceReverb();
            prevActive = active;
        }
    }
}
