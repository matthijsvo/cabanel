﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

// Allows debug messages from within native C++ code (i.c. the Cabanel library) to conveniently show up in the Unity Editor console.
// Use in C++:
//      DebugLog(<string>);
//
// ATTACH TO SOME COMPONENT IN THE SCENE
//
// Accompanied by code in Misc.h
// 
// Source: https://answers.unity.com/questions/30620/how-to-debug-c-dll-code.html
public class DebugCallback : MonoBehaviour {

    private delegate void DbgCallback(string message);

    [DllImport("AudioPluginCabanel")]
    private static extern void RegisterDebugCallback(DbgCallback callback);

    //[RuntimeInitializeOnLoadMethod]
    private void Start()
    {
        Debug.Log("Initialized link for plug-in debug messages");
        RegisterDebugCallback(new DbgCallback(DebugMethod));
    }

    private static void DebugMethod(string message)
    {
        Debug.Log("AudioPluginCabanel: " + message);
    }

}
